*** Settings ***
Library     SeleniumLibrary
Library     RequestsLibrary
Library     Collections
Library     String
Library     OperatingSystem
Library     FakerLibrary        locale=pt

Resource    Elements.robot

*** Keywords ***
## Setup e teardown ##
Final do teste
    Close Browser
    Capture Page Screenshot
#

## Keywords dos Testes ##    
# Login no site
Dado que eu acesso a pagina do SWAGLABS
    Open Browser    ${URL.SAUCE_DEMO}      Chrome   options=${OPTIONS}
    Set Window Size     1366      768
    Set Selenium Implicit Wait    15

Quando logo com um usuário valido
    Input text      ${TEXT.USER_NAME}       ${exemplo_sauce.user_correto}      
    Input text      ${TEXT.PASSWORD}        ${exemplo_sauce.password}     
    Click Button    ${BUTTON.LOGIN}

Entao valido se entrou no sistema
    Location Should Be      ${URL.SAUCE_DEMO_INVENTORY}
#
# Login no site com usuario bloqueado
Quando logo com um usuário bloqueado
    # Logando no sistema
    Input text      ${TEXT.USER_NAME}       ${exemplo_sauce.user_locked}      
    Input text      ${TEXT.PASSWORD}        ${exemplo_sauce.password}     
    Click Button    ${BUTTON.LOGIN}

Entao valido se retorna a mensagem usuário bloqueado
    Element Should Contain  ${SPAN.RETORNO_ACAO}    ${mensagem.usuario_bloqueado}   ignore_case=true    message=Mensagem divergente da esperada
#
# Escolha de um produto no site automation practice    
Dado que eu acesso a pagina do Automation Practice
    # Abrir o navegador com a URL do site
    Open Browser    ${URL.AUTOMATION_PRACTICE}     Chrome   options=${OPTIONS}
    Set Window Size     1366      768
    Set Selenium Implicit Wait    15

Quando realizo uma busca pelo produto "Faded Short Sleeve T-shirts" e adiciono no carrinho
    # Colocar o produto que pretende buscar no text Busca
    Input Text  ${TEXT.AUTOMATION_PRACTICE_SOURCE}       Faded Short Sleeve T-shirts

    # Clicar no botao Search
    Click BUTTON    ${BUTTON.PRACTICE_SOURCE}

    # Habilitar o Botao ADDD TO CARD
    Mouse Over      ${IMG.FADEDSHORTSLEEVETSHIRTS}

    # Clicar no Botao
    Click Element   ${BUTTON.PRACTICE_ADD_TO_CARD}    

    # Conferir se abriu o popUp de Adição no carrinho
    Page Should Contain      ${mensagem.practice_product_add_sucessfully} 

    # Clicar no botao Proceed to checkout
    Click Link  ${BUTTON.PRACTICE_PROCEED_CHECKOUT} 

Entao valido se esse produto foi adicionado com sucesso
    # Confimar se o produto foi incluido no carrinho
    Press Keys      None        ARROW_DOWN 
    Page Should Contain Element     ${LINK.PRACTICE_FADED_SHORT_SLEEVE_TSHIRTS}       message=Produto nao adicionado no carrinho
