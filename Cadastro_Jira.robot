*** Settings ***
Library     RequestsLibrary
Library     String
Library     OperatingSystem
Library     DateTime

*** Variable ***
# Variaveis do processo
${id_projeto}    20457       ## Id do projeto => WTT Testes
${id_TestPlan}   263921      ## Id do Plano de Teste =>  Plano de Teste

&{API}  HOST=https://xray.cloud.xpand-it.com
   ...  AUTENTICACAO=/api/v1/authenticate
   ...  IMPORT_TEST_EXECUTION=/api/v1/import/execution/robot
   ...  GRAPHQL=/api/v1/graphql

${client_id}         SEU CLIENT ID
${client_secret}     SEU CLIENT SECRET

${BODY_AUTENTICACAO_XRAY}    { "client_id": "${client_id}", "client_secret": "${client_secret}" }

*** Test Case ***
Cadastro no Jira
   #############    CRIA SESSÃO NO JIRA                                 ############# 
    # Cria a sessão do servidor Xray Cloud
    Create Session    alias=api_xray       url=${API.HOST}      disable_warnings=True

   #############    GERA O TOKEN DE ACESSO                              ############# 
    # Realiza o Post para gerar o token de autenticação e o salva
    ${headers_aut}    Create Dictionary    Content-type=application/json
    ${Resposta}       POST On Session      alias=api_xray       url=${API.AUTENTICACAO}     data=${BODY_AUTENTICACAO_XRAY}     headers=${headers_aut}
    ${token}          Remove String        ${Resposta.text}   "

   #############    CRIA O TEST EXECUTION                               ############# 
    # Gera um nome dinamico para TestExecute
    ${data_atual}           Get Current Date    result_format=%d.%m.%Y %H:%M
    ${nome_testExectution}  Set variable        Execucao Automatica - ${data_atual}

    # Gera o Body para a criacao do TestExecution
    ${GraphQL_testExectution}  Format String    ${EXECDIR}${/}\Docs${/}\Templates${/}CreateTestExecution.json
                          ...  nome_testExecution=${nome_testExectution}             
                          ...  id_projeto=${id_projeto}

    # Executa o POST para a criacao do TestExecution
    &{headers_import}   Create Dictionary   Authorization=Bearer ${token}         Content-type=application/json
    ${RESPONSE}         POST On Session     alias=api_xray  url=${API.GRAPHQL}    data=${GraphQL_testExectution}     headers=${headers_import}

    # Salvo em variaveis o Id e o Key do TestExecution criado
    ${result_TestExecution}     Set Variable    ${RESPONSE.json()}
    ${TestExecution_id}         Set Variable    ${result_TestExecution["data"]["createTestExecution"]["testExecution"]["issueId"]}
    ${TestExecution_key}        Set Variable    ${result_TestExecution["data"]["createTestExecution"]["testExecution"]["jira"]["key"]}

    Log to console    \n
    Log to console    TestExecution criado com o nome: ${nome_testExectution}

   #############    ADICIONAR O TEST EXECUTION EM UM TEST PLAN          ############# 
    # Gera o Body para a criacao do adicionar o TestExecution num determinado TestPlan
    ${GraphQL_addTestExecutionsToTestPlan}  Format String    ${EXECDIR}${/}Docs${/}Templates${/}addTestExecutionsToTestPlan.json
                                       ...  id_TestPlan=${id_TestPlan}             
                                       ...  id_testExecution=${TestExecution_id}

    # Executa o POST para adicionar o TestExecution num determinado TestPlan
    &{headers_import}   Create Dictionary   Authorization=Bearer ${token}         Content-type=application/json
    ${RESPONSE}         POST On Session     alias=api_xray  url=${API.GRAPHQL}    data=${GraphQL_addTestExecutionsToTestPlan}     headers=${headers_import}
    

   #############    IMPORTAR O TEST RESULT AO TEXT EXECUTION CRIADO     ############# 
    # Realiza o Post de import do testResult com o token de autenticação
    ${XML}              Get File            .${/}log${/}output.xml
    &{headers_import}   Create Dictionary   Authorization=Bearer ${token}   Content-type=application/xml
    &{params}           Create Dictionary   testExecKey=${TestExecution_key}

    ${RESPONSE}         POST On Session        alias=api_xray  url=${API.IMPORT_TEST_EXECUTION}    data=${XML}    params=${params}    headers=${headers_import}

    Confere sucesso na requisição   ${RESPONSE}
#

*** Keywords ***
Confere sucesso na requisição
    [Arguments]       ${RESPONSE}
    Should Be True   '${RESPONSE.status_code}'=='200' or '${RESPONSE.status_code}'=='201'       msg=Erro na requisição! Verifique: ${RESPONSE}





