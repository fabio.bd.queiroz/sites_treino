*** Settings ***
Resource     keywords.robot

Test Teardown   Final do teste

Documentation   Praticas em sites de treinamento

*** Test Case ***
Login no site
    [tags]  SWAGLABS
     Dado que eu acesso a pagina do SWAGLABS
     Quando logo com um usuário valido
     Entao valido se entrou no sistema

Login no site com usuario bloqueado
    [tags]  SWAGLABS
     Dado que eu acesso a pagina do SWAGLABS
     Quando logo com um usuário bloqueado
     Entao valido se retorna a mensagem usuário bloqueado

Escolha de um produto no site automation practice
    [tags]  Automation_Demo
     Dado que eu acesso a pagina do Automation Practice
     Quando realizo uma busca pelo produto "Faded Short Sleeve T-shirts" e adiciono no carrinho
     Entao valido se esse produto foi adicionado com sucesso